package tsatsubii

import (
	"github.com/google/uuid"
)

type PermissionKey string

const (
	PermNone PermissionKey = "none"
)

type Permission struct {
	Model
	RoleID uuid.UUID     `gorm:"type:uuid;uniqueIndex:permindex" json:"role_id"`
	Key    PermissionKey `gorm:"uniqueIndex:permindex" json:"permission"`
}

type Permissions []Permission

func HasPermission(roleID uuid.UUID, key PermissionKey) bool {
	var count int64

	if roleID == SystemRoleID() {
		return true
	}

	if key == PermNone {
		return true
	}

	result := database.Model(
		&Permission{},
	).Where(
		"role_id = ? AND key = ?",
		roleID,
		key,
	).Count(
		&count,
	)

	if result.Error != nil {
		return false
	}

	return count > 0
}

func GrantPermission(roleID uuid.UUID, key PermissionKey) {
	if HasPermission(roleID, key) {
		return
	}

	perm := Permission{
		RoleID: roleID,
		Key:    key,
	}

	if err := database.Create(&perm).Error; err != nil {
		logger.Error(err.Error())
	}
}

func RevokePermission(roleID uuid.UUID, key PermissionKey) {
	if !HasPermission(roleID, key) {
		return
	}

	database.Where(
		"role_id = ? AND key = ?",
		roleID,
		key,
	).Delete(
		&Permission{},
	)
}
