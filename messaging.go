package tsatsubii

import (
	"fmt"
	"os"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

var amqpConn *amqp.Connection

func AmqpConnection() *amqp.Connection {
	if amqpConn == nil {
		initAmqp()
	}

	return amqpConn
}

func initAmqp() {
	EnsureEnv("RABBITMQ_HOST")
	EnsureEnv("RABBITMQ_USER")
	EnsureEnv("RABBITMQ_PASSWORD")
	EnsureEnv("RABBITMQ_PORT")

	connStr := fmt.Sprintf(
		"amqp://%s:%s@%s:%s",
		os.Getenv("RABBITMQ_USER"),
		os.Getenv("RABBITMQ_PASSWORD"),
		os.Getenv("RABBITMQ_HOST"),
		os.Getenv("RABBITMQ_PORT"),
	)

	// Allow time for the RabbitMQ container to come up.
	for i := 0; i < 5; i++ {
		if conn, err := amqp.Dial(connStr); err == nil {
			amqpConn = conn
			logger.Info("Connected to RabbitMQ")
			return
		}

		logger.Warn("Connection to RabbitMQ failed. Retrying...")
		time.Sleep(time.Second * 3)
	}

	logger.Fatal("Unable to connect to RabbitMQ. Giving up.")
}

func PublishOnQueue(queueName string, data []byte) error {
	ch, _ := amqpConn.Channel()
	defer ch.Close()

	queue, _ := ch.QueueDeclare(
		queueName,
		false,
		false,
		false,
		false,
		nil,
	)

	err := ch.Publish(
		"",
		queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        data,
		},
	)

	return err
}

func SubscribeToQueue(
	queueName string,
	consumerName string,
	handlerFunc func(amqp.Delivery),
) error {
	ch, _ := amqpConn.Channel()

	queue, err := ch.QueueDeclare(
		queueName,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := ch.Consume(
		queue.Name,
		consumerName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	forever := make(chan bool)
	go func() {
		for d := range msgs {
			handlerFunc(d)
		}
	}()
	<-forever

	return nil
}
