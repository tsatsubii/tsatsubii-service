package tsatsubii

import (
	"github.com/google/uuid"
)

type IModel interface {
	Prepare()
	Validate() error
}

type IRepository interface {
	Get(id uuid.UUID)
	Save(form IForm) (IModel, error)
	FindAll() []IModel
}

type Model struct {
	ID uuid.UUID `json:"id" gorm:"type:uuid;primaryKey;default:uuid_generate_v4()"`
}
