package tsatsubii

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/ken109/gin-jwt"
)

func PermissionMiddleware(key PermissionKey) gin.HandlerFunc {
	return func(c *gin.Context) {
		claims := jwt.GetClaims(c)
		roleID, _ := uuid.Parse(claims["role_id"].(string))

		if !HasPermission(roleID, key) {
			c.AbortWithStatusJSON(
				http.StatusUnauthorized,
				gin.H{
					"message": "insufficient permissions",
				},
			)

			return
		}

		c.Next()
	}
}
