package tsatsubii

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

func Init() {
	if err := godotenv.Load(); err != nil {
		panic("No .env file found")
	}

	// Make sure logger remains initialized first.
	initLogger()
	initDatabase()
	initAmqp()
	initWeb()
}

func Start() {
	listen := fmt.Sprintf(
		"%s:%s",
		os.Getenv("HOST"),
		os.Getenv("PORT"),
	)

	router.Run(listen)
}
