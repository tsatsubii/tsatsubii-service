package tsatsubii

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	// TODO: this should not be hard-coded
	AuthUrl string = "http://auth:8000/sys"
)

type SysResponse struct {
	Token        string `json:"token"`
	RefreshToken string `json:"refresh_token"`
}

type ApiCaller struct {
	Token        string
	RefreshToken string
}

func (a *ApiCaller) Login() {
	var sysResponse SysResponse

	jsonData, _ := json.Marshal(
		SystemForm{
			Secret: SystemSecret,
		},
	)

	resp, err := http.Post(
		AuthUrl,
		"application/json",
		bytes.NewBuffer(jsonData),
	)
	if err != nil {
		logger.Error(err.Error())
		return
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&sysResponse); err != nil {
		logger.Error(err.Error())
		return
	}

	a.Token = sysResponse.Token
	a.RefreshToken = sysResponse.RefreshToken
}

func (a *ApiCaller) Refresh() {
	var sysResponse SysResponse

	url := fmt.Sprintf(
		"http://auth:8000/refresh?token=%s",
		a.RefreshToken,
	)

	resp, err := http.Get(url)
	if err != nil {
		logger.Error(err.Error())
		return
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&sysResponse); err != nil {
		logger.Error(err.Error())
		return
	}

	a.Token = sysResponse.Token
	a.RefreshToken = sysResponse.RefreshToken
}

func (a *ApiCaller) Call(method string, url string, data []byte) *http.Response {
	if a.Token == "" {
		a.Login()
	}

	auth := fmt.Sprintf("Bearer %s", a.Token)
	client := &http.Client{}
	req, err := http.NewRequest(method, url, bytes.NewBuffer(data))
	if err != nil {
		return nil
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", auth)
	resp, err := client.Do(req)
	if err != nil {
		return nil
	}

	return resp
}

func (a *ApiCaller) CallAs(
	token string,
	method string,
	url string,
	data []byte,
) *http.Response {
	auth := fmt.Sprintf("Bearer %s", token)
	client := &http.Client{}
	req, err := http.NewRequest(method, url, bytes.NewBuffer(data))
	if err != nil {
		return nil
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", auth)
	resp, err := client.Do(req)
	if err != nil {
		return nil
	}

	return resp
}
