package tsatsubii

import (
	"fmt"
	"os"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var database *gorm.DB

func Database() *gorm.DB {
	if database == nil {
		initDatabase()
	}

	return database
}

func initDatabase() {
	// For now we just call initPg. Later, if we want to support other
	// databases as well, we can extend this.
	initPg()
	migrate()
}

func initPg() {
	EnsureEnv("POSTGRES_HOST")
	EnsureEnv("POSTGRES_USER")
	EnsureEnv("POSTGRES_PASSWORD")
	EnsureEnv("POSTGRES_DB")

	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("POSTGRES_HOST"),
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_DB"),
	)

	// The Gin container may come up before the Postgresql one, so we try
	// to connect a few times before giving up.
	for i := 0; i < 5; i++ {
		if db, err := gorm.Open(
			postgres.Open(dsn),
			&gorm.Config{},
		); err == nil {
			database = db
			logger.Info("Connected to PostgreSQL")
			return
		}

		logger.Warn("Connection to PostgreSQL failed. Retrying...")
		time.Sleep(time.Second * 3)
	}

	logger.Fatal("Unable to connect to PostgreSQL. Giving up.")
}

func migrate() {
	database.AutoMigrate(&Permission{})
}
