package tsatsubii

import (
	"github.com/google/uuid"
)

const (
	// TODO: let the build system generate these
	sysAdminRoleID = "2c07ccde-60a5-4946-aa15-135ed19db333"
	systemRoleID   = "29e1e63b-ca47-4b90-8f81-aa19ec70540"
)

func SysAdminRoleID() uuid.UUID {
	roleID, _ := uuid.Parse(sysAdminRoleID)
	return roleID
}

func SystemRoleID() uuid.UUID {
	roleID, _ := uuid.Parse(systemRoleID)
	return roleID
}
