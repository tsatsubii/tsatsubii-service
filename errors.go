package tsatsubii

type ValidationError struct {
	Errors []string
}

func (e *ValidationError) Error() string {
	return "validation errors"
}

func (e *ValidationError) AddError(err string) {
	e.Errors = append(e.Errors, err)
}

func (e *ValidationError) HasErrors() bool {
	return len(e.Errors) > 0
}

type InternalError struct{}

func (e *InternalError) Error() string {
	return "internal error"
}
