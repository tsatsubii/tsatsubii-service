package tsatsubii

import (
	"os"

	"go.uber.org/zap"
)

func EnsureEnv(key string) {
	if val, ok := os.LookupEnv(key); !ok || val == "" {
		logger.Fatal(
			"Environment variable not set",
			zap.String("env", key),
		)
	}
}
