package tsatsubii

import (
	"os"

	"github.com/gin-gonic/gin"
	"github.com/ken109/gin-jwt"
)

var router *gin.Engine
var authRouter gin.IRoutes

func initWeb() {
	EnsureEnv("HOST")
	EnsureEnv("PORT")
	EnsureEnv("JWT_REALM")

	router = gin.Default()
	authRouter = router.Group("/api")
	authRouter.Use(jwt.MustVerify(os.Getenv("JWT_REALM")))
}

func AddRoute(method string, path string, handler gin.HandlerFunc) {
	router.Handle(method, path, handler)
}

func AddAuthRoute(
	method string,
	path string,
	key PermissionKey,
	handler gin.HandlerFunc,
) {

	authRouter.Handle(
		method,
		path,
		PermissionMiddleware(key),
		handler,
	)
}
