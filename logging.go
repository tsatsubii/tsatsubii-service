package tsatsubii

import (
	"go.uber.org/zap"
)

var logger *zap.Logger

func Logger() *zap.Logger {
	if logger != nil {
		initLogger()
	}

	return logger
}

func initLogger() {
	// At this point we just return a development logger. Later, we should
	// check if we are in development or production and create an appropriate
	// logger.
	logger, _ = zap.NewDevelopment()
	defer logger.Sync()
}
